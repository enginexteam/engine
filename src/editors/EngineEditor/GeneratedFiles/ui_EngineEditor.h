/********************************************************************************
** Form generated from reading UI file 'EngineEditor.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ENGINEEDITOR_H
#define UI_ENGINEEDITOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QOpenGLWidget>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EngineEditorClass
{
public:
    QAction *actionClear;
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionSave_As;
    QAction *actionOpen_Selection;
    QAction *actionSave_Selection_As;
    QAction *actionMake_Pack;
    QAction *actionQuit;
    QAction *actiontest_resent;
    QAction *actionOptions;
    QAction *actionValidate;
    QAction *actionSummary_Info;
    QAction *actionEdit;
    QAction *actionSelection;
    QAction *actionVisibility;
    QAction *actionProperties;
    QAction *actionMulti_Rename;
    QAction *actionCheck_Updates;
    QAction *actionAbout_me;
    QAction *actionHighlight_Texture;
    QAction *actionClear_Debug_Draw;
    QAction *actionExport_entire_scene;
    QAction *actionExport_selection;
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QToolButton *toolButton;
    QToolButton *toolButton_2;
    QTabWidget *tabWidget;
    QWidget *tab;
    QOpenGLWidget *renderFrameGL;
    QWidget *tab_2;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuOpen_Recent;
    QMenu *menuScene;
    QMenu *menuTools;
    QMenu *menuCOmpile;
    QMenu *menuObjects;
    QMenu *menuImages;
    QMenu *menuSOunds;
    QMenu *menuLight_Anim_Editor;
    QMenu *menuObject_List;
    QMenu *menuSettings;
    QMenu *menuAbout;

    void setupUi(QMainWindow *EngineEditorClass)
    {
        if (EngineEditorClass->objectName().isEmpty())
            EngineEditorClass->setObjectName(QString::fromUtf8("EngineEditorClass"));
        EngineEditorClass->resize(1371, 771);
        actionClear = new QAction(EngineEditorClass);
        actionClear->setObjectName(QString::fromUtf8("actionClear"));
        actionOpen = new QAction(EngineEditorClass);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        actionSave = new QAction(EngineEditorClass);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        actionSave_As = new QAction(EngineEditorClass);
        actionSave_As->setObjectName(QString::fromUtf8("actionSave_As"));
        actionOpen_Selection = new QAction(EngineEditorClass);
        actionOpen_Selection->setObjectName(QString::fromUtf8("actionOpen_Selection"));
        actionSave_Selection_As = new QAction(EngineEditorClass);
        actionSave_Selection_As->setObjectName(QString::fromUtf8("actionSave_Selection_As"));
        actionMake_Pack = new QAction(EngineEditorClass);
        actionMake_Pack->setObjectName(QString::fromUtf8("actionMake_Pack"));
        actionQuit = new QAction(EngineEditorClass);
        actionQuit->setObjectName(QString::fromUtf8("actionQuit"));
        actiontest_resent = new QAction(EngineEditorClass);
        actiontest_resent->setObjectName(QString::fromUtf8("actiontest_resent"));
        actionOptions = new QAction(EngineEditorClass);
        actionOptions->setObjectName(QString::fromUtf8("actionOptions"));
        actionValidate = new QAction(EngineEditorClass);
        actionValidate->setObjectName(QString::fromUtf8("actionValidate"));
        actionSummary_Info = new QAction(EngineEditorClass);
        actionSummary_Info->setObjectName(QString::fromUtf8("actionSummary_Info"));
        actionEdit = new QAction(EngineEditorClass);
        actionEdit->setObjectName(QString::fromUtf8("actionEdit"));
        actionSelection = new QAction(EngineEditorClass);
        actionSelection->setObjectName(QString::fromUtf8("actionSelection"));
        actionVisibility = new QAction(EngineEditorClass);
        actionVisibility->setObjectName(QString::fromUtf8("actionVisibility"));
        actionProperties = new QAction(EngineEditorClass);
        actionProperties->setObjectName(QString::fromUtf8("actionProperties"));
        actionMulti_Rename = new QAction(EngineEditorClass);
        actionMulti_Rename->setObjectName(QString::fromUtf8("actionMulti_Rename"));
        actionCheck_Updates = new QAction(EngineEditorClass);
        actionCheck_Updates->setObjectName(QString::fromUtf8("actionCheck_Updates"));
        actionAbout_me = new QAction(EngineEditorClass);
        actionAbout_me->setObjectName(QString::fromUtf8("actionAbout_me"));
        actionHighlight_Texture = new QAction(EngineEditorClass);
        actionHighlight_Texture->setObjectName(QString::fromUtf8("actionHighlight_Texture"));
        actionClear_Debug_Draw = new QAction(EngineEditorClass);
        actionClear_Debug_Draw->setObjectName(QString::fromUtf8("actionClear_Debug_Draw"));
        actionExport_entire_scene = new QAction(EngineEditorClass);
        actionExport_entire_scene->setObjectName(QString::fromUtf8("actionExport_entire_scene"));
        actionExport_selection = new QAction(EngineEditorClass);
        actionExport_selection->setObjectName(QString::fromUtf8("actionExport_selection"));
        centralWidget = new QWidget(EngineEditorClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(0, 590, 1111, 161));
        scrollArea = new QScrollArea(groupBox);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setGeometry(QRect(10, 20, 1091, 131));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 1089, 129));
        scrollArea->setWidget(scrollAreaWidgetContents);
        toolButton = new QToolButton(centralWidget);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setGeometry(QRect(1210, 130, 71, 22));
        toolButton_2 = new QToolButton(centralWidget);
        toolButton_2->setObjectName(QString::fromUtf8("toolButton_2"));
        toolButton_2->setGeometry(QRect(1240, 190, 71, 22));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 1111, 591));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        renderFrameGL = new QOpenGLWidget(tab);
        renderFrameGL->setObjectName(QString::fromUtf8("renderFrameGL"));
        renderFrameGL->setGeometry(QRect(0, 0, 1111, 581));
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        tabWidget->addTab(tab_2, QString());
        EngineEditorClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(EngineEditorClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1371, 26));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuOpen_Recent = new QMenu(menuFile);
        menuOpen_Recent->setObjectName(QString::fromUtf8("menuOpen_Recent"));
        menuScene = new QMenu(menuBar);
        menuScene->setObjectName(QString::fromUtf8("menuScene"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        menuCOmpile = new QMenu(menuBar);
        menuCOmpile->setObjectName(QString::fromUtf8("menuCOmpile"));
        menuObjects = new QMenu(menuBar);
        menuObjects->setObjectName(QString::fromUtf8("menuObjects"));
        menuImages = new QMenu(menuBar);
        menuImages->setObjectName(QString::fromUtf8("menuImages"));
        menuSOunds = new QMenu(menuBar);
        menuSOunds->setObjectName(QString::fromUtf8("menuSOunds"));
        menuLight_Anim_Editor = new QMenu(menuBar);
        menuLight_Anim_Editor->setObjectName(QString::fromUtf8("menuLight_Anim_Editor"));
        menuObject_List = new QMenu(menuBar);
        menuObject_List->setObjectName(QString::fromUtf8("menuObject_List"));
        menuSettings = new QMenu(menuBar);
        menuSettings->setObjectName(QString::fromUtf8("menuSettings"));
        menuAbout = new QMenu(menuBar);
        menuAbout->setObjectName(QString::fromUtf8("menuAbout"));
        EngineEditorClass->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuScene->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuCOmpile->menuAction());
        menuBar->addAction(menuObjects->menuAction());
        menuBar->addAction(menuImages->menuAction());
        menuBar->addAction(menuSOunds->menuAction());
        menuBar->addAction(menuLight_Anim_Editor->menuAction());
        menuBar->addAction(menuObject_List->menuAction());
        menuBar->addAction(menuSettings->menuAction());
        menuBar->addAction(menuAbout->menuAction());
        menuFile->addAction(actionClear);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuFile->addAction(actionSave_As);
        menuFile->addSeparator();
        menuFile->addAction(actionOpen_Selection);
        menuFile->addAction(actionSave_Selection_As);
        menuFile->addSeparator();
        menuFile->addAction(actionMake_Pack);
        menuFile->addSeparator();
        menuFile->addAction(menuOpen_Recent->menuAction());
        menuFile->addSeparator();
        menuFile->addAction(actionQuit);
        menuOpen_Recent->addAction(actiontest_resent);
        menuScene->addAction(actionOptions);
        menuScene->addSeparator();
        menuScene->addAction(actionValidate);
        menuScene->addSeparator();
        menuScene->addAction(actionSummary_Info);
        menuScene->addAction(actionHighlight_Texture);
        menuScene->addSeparator();
        menuScene->addAction(actionClear_Debug_Draw);
        menuScene->addSeparator();
        menuScene->addAction(actionExport_entire_scene);
        menuScene->addAction(actionExport_selection);
        menuTools->addAction(actionEdit);
        menuTools->addAction(actionSelection);
        menuTools->addAction(actionVisibility);
        menuTools->addAction(actionProperties);
        menuTools->addAction(actionMulti_Rename);
        menuAbout->addAction(actionCheck_Updates);
        menuAbout->addAction(actionAbout_me);

        retranslateUi(EngineEditorClass);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(EngineEditorClass);
    } // setupUi

    void retranslateUi(QMainWindow *EngineEditorClass)
    {
        EngineEditorClass->setWindowTitle(QApplication::translate("EngineEditorClass", "EngineEditor", nullptr));
        actionClear->setText(QApplication::translate("EngineEditorClass", "Clear", nullptr));
        actionOpen->setText(QApplication::translate("EngineEditorClass", "Open...", nullptr));
        actionSave->setText(QApplication::translate("EngineEditorClass", "Save", nullptr));
        actionSave_As->setText(QApplication::translate("EngineEditorClass", "Save As...", nullptr));
        actionOpen_Selection->setText(QApplication::translate("EngineEditorClass", "Open Selection...", nullptr));
        actionSave_Selection_As->setText(QApplication::translate("EngineEditorClass", "Save Selection As...", nullptr));
        actionMake_Pack->setText(QApplication::translate("EngineEditorClass", "Make Pack...", nullptr));
        actionQuit->setText(QApplication::translate("EngineEditorClass", "Quit", nullptr));
        actiontest_resent->setText(QApplication::translate("EngineEditorClass", "test_resent", nullptr));
        actionOptions->setText(QApplication::translate("EngineEditorClass", "Options", nullptr));
        actionValidate->setText(QApplication::translate("EngineEditorClass", "Validate", nullptr));
        actionSummary_Info->setText(QApplication::translate("EngineEditorClass", "Summary Info...", nullptr));
        actionEdit->setText(QApplication::translate("EngineEditorClass", "Edit", nullptr));
        actionSelection->setText(QApplication::translate("EngineEditorClass", "Selection", nullptr));
        actionVisibility->setText(QApplication::translate("EngineEditorClass", "Visibility", nullptr));
        actionProperties->setText(QApplication::translate("EngineEditorClass", "Properties", nullptr));
        actionMulti_Rename->setText(QApplication::translate("EngineEditorClass", "Multi Rename", nullptr));
        actionCheck_Updates->setText(QApplication::translate("EngineEditorClass", "Check Updates", nullptr));
        actionAbout_me->setText(QApplication::translate("EngineEditorClass", "About Me", nullptr));
        actionHighlight_Texture->setText(QApplication::translate("EngineEditorClass", "Highlight Texture...", nullptr));
        actionClear_Debug_Draw->setText(QApplication::translate("EngineEditorClass", "Clear Debug Draw...", nullptr));
        actionExport_entire_scene->setText(QApplication::translate("EngineEditorClass", "Export entire scene", nullptr));
        actionExport_selection->setText(QApplication::translate("EngineEditorClass", "Export selection", nullptr));
        groupBox->setTitle(QApplication::translate("EngineEditorClass", "Library", nullptr));
        toolButton->setText(QApplication::translate("EngineEditorClass", "Options", nullptr));
        toolButton_2->setText(QApplication::translate("EngineEditorClass", "Macro", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("EngineEditorClass", "Tab 1", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("EngineEditorClass", "Tab 2", nullptr));
        menuFile->setTitle(QApplication::translate("EngineEditorClass", "File", nullptr));
        menuOpen_Recent->setTitle(QApplication::translate("EngineEditorClass", "Open Recent", nullptr));
        menuScene->setTitle(QApplication::translate("EngineEditorClass", "Scene", nullptr));
        menuTools->setTitle(QApplication::translate("EngineEditorClass", "Tools", nullptr));
        menuCOmpile->setTitle(QApplication::translate("EngineEditorClass", "Compile", nullptr));
        menuObjects->setTitle(QApplication::translate("EngineEditorClass", "Objects", nullptr));
        menuImages->setTitle(QApplication::translate("EngineEditorClass", "Images", nullptr));
        menuSOunds->setTitle(QApplication::translate("EngineEditorClass", "Sounds", nullptr));
        menuLight_Anim_Editor->setTitle(QApplication::translate("EngineEditorClass", "Light Anim Editor", nullptr));
        menuObject_List->setTitle(QApplication::translate("EngineEditorClass", "Object List", nullptr));
        menuSettings->setTitle(QApplication::translate("EngineEditorClass", "Settings", nullptr));
        menuAbout->setTitle(QApplication::translate("EngineEditorClass", "About", nullptr));
    } // retranslateUi

};

namespace Ui {
    class EngineEditorClass: public Ui_EngineEditorClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ENGINEEDITOR_H
