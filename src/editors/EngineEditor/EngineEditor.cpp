#include "EngineEditor.h"
//#include "Builder.h"

#include "xrCore/Threading/Event.hpp"
#include "xrEngine/main.h"
#include "xrEngine/device.h"
#include "xrEngine/XR_IOConsole.h"
#include "xrEngine/xr_ioc_cmd.h"
#include "SDL.h"

EngineEditor::EngineEditor(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	connect(ui.actionClear, SIGNAL(triggered()), this, SLOT(clearScene()));

#ifdef DEBUG
	xrDebug::Initialize();
#endif
	thread_spawn(UIThreadProcess, "OpenXRay Editor UI Thread", 0, nullptr);
	RunApplication();

}

void EngineEditor::UIThreadProcess(void*)
{
	// engine core init
	Core.Initialize("EngineEditor", nullptr, nullptr, true, nullptr);

#ifdef XR_X64
	Device.m_sdlWnd = (SDL_Window*)nullptr;//ui.renderFrameGL;
#else
	Device.m_sdlWnd = (SDL_Window*)ui.renderFrameGL;
#endif
}


void EngineEditor::clearScene()
{
	//Clear();
}
