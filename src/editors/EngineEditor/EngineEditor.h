#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_EngineEditor.h"

#include "Common/Common.hpp"
#include "Common/FSMacros.hpp"
#include "xrCore/xrCore.h"
#include "xrCommon/xr_vector.h"
#include "xrCommon/xr_string.h"

class EngineEditor : public QMainWindow
{
	Q_OBJECT

public:
	EngineEditor(QWidget *parent = Q_NULLPTR);

	void clearScene();
	static void UIThreadProcess(void*);

private:
	Ui::EngineEditorClass ui;
};
