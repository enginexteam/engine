#include "EngineEditor.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	EngineEditor w;
	w.show();

	return a.exec();
}
